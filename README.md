# gc-api-android
Android lib for Swych B2B Gift card REST API

REST API https://developer.swychover.io/docs/services/swych-global-b2b-api/operations/access-token

# How to include this lib
There are 2 modules: app & api.

- app module is for demo/testing purpose only.
- api module is the main api/lib module.

## include :api module in your project
- See example project https://bitbucket.org/swychkiosk/gc-api-android-example
- ```module.gradle```: a utility to allow app to include external modules. Provided that your app project is located under the same parent of gc-api-android, add below lines to ```settings.gradle```
```gradle
apply from: '../gc-api-android/module.gradle'
includeModule('api', '../gc-api-android/')
```
## include .aar file in build.gradle
TBD

# Usage notes
There 2 ways to use:

- Direct call to REST API: methods with api prefix
- Wrapper call with session management (Recommend)


# Store & inject api keys
Below is a recommended way store & inject keys from the app.

## Store
- Store in global ```~/.gradle/gradle.properties```, which is not committed to repo. Note: local/project gradle.properties should be committed to repo, it's not a good choice.
- Since we have many api keys and key sets, we store each key set in a json string as below:
```gradle
swych_api_keys_debug="{ \\"ocpApimSubscriptionKey\\":\\"xxx\\", ... , \\"client_id\\":\\"xxx\\" }"
swych_api_keys_release="{ \\"ocpApimSubscriptionKey\\":\\"xxx\\", ... , \\"client_id\\":\\"xxx\\" }"
```

## Inject
1. Transfers keys from gradle.properties to app ```BuildConfig``` object during compile-time in build.gradle
```
debug {
  if (project.hasProperty("swych_api_keys_debug"))
    buildConfigField "String", "swych_api_keys", swych_api_keys_debug
...
}
```
- Note: add above code snippet to all of your buildTypes 

2. The lib provides ```setApiKeys``` function for app to inject the keys.
During runtime initialization, the app should inject the keys using below code:
```java
Session.setApiKeys(BuildConfig.swych_api_keys)
```

# Dependencies
- Kotlin https://developer.android.com/kotlin
- Logging Interceptor for debug https://github.com/square/okhttp/tree/master/okhttp-logging-interceptor
- Retrofit: Type-safe HTTP client for Android and Java https://github.com/square/retrofit
- Moshi: A modern JSON library for Kotlin and Java https://github.com/square/moshi
- RxAndroid: Reactive Extensions for Android https://github.com/ReactiveX/RxAndroid
- RxJava – Reactive Extensions for the JVM – a library for composing asynchronous and event-based programs using observable sequences for the Java VM https://github.com/ReactiveX/RxJava
