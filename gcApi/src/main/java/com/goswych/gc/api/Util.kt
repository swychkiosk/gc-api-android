package com.goswych.gc.api

import android.util.Log
import retrofit2.HttpException
import java.net.SocketTimeoutException
import java.net.UnknownHostException

object Util {
    val TAG = "GcApi"
    fun log(text: String) = Log.d(TAG, text)

    /**
     * Handle common error internally before calling callback
     * return null if you want to stop the callback
     */
    fun errorHandler(errorThrowable: Throwable): Boolean? {
        if (errorThrowable is SocketTimeoutException) {
            Log.w(TAG, "timeout! ${errorThrowable.message}")
            // TODO: implement exponential backoff retry here. Note: since timeout issue is fixed, this is just nice-to-have
        }
        else if (errorThrowable is UnknownHostException) {
            Log.w(TAG, "No internet! UnknownHostException ${errorThrowable.message}")
        }
        return true
    }

    fun getHttpErrorBody(error: Throwable): String? {
        if (error is HttpException) {
            val errorBody = (error as? HttpException)?.response()?.errorBody()?.string() ?: return null
            if (errorBody.isNullOrBlank()) return null
            return errorBody
        }
        return null
    }
}