package com.goswych.gc.api

import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.CallAdapter
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory

object Factory {
    val moshi by lazy { Moshi.Builder().add(KotlinJsonAdapterFactory()).build() }

    @JvmStatic
    inline fun <reified T> create(baseUrl: String, callAdapter: CallAdapter.Factory? = null): T {
        Util.log("- init $baseUrl")
        val client = OkHttpClient.Builder()
            .addInterceptor(HttpLoggingInterceptor().apply {
                level = if (BuildConfig.DEBUG) HttpLoggingInterceptor.Level.BODY else HttpLoggingInterceptor.Level.NONE
            })
            .build()

        // No need: addCallAdapterFactory(CoroutineCallAdapterFactory())
        val builder = Retrofit.Builder()
            .addConverterFactory(MoshiConverterFactory.create(moshi))
            .baseUrl(baseUrl)
            .client(client)
        if (callAdapter != null) builder.addCallAdapterFactory(callAdapter)

        return builder.build().create(T::class.java)
    }
}