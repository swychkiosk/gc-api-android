/*
 * Copyright 2019 Swych Inc.
 * Author: Trung Vo
 */

package com.goswych.gc.api.models

import com.goswych.gc.api.Factory
import com.goswych.gc.api.Util
import com.squareup.moshi.JsonAdapter
import com.squareup.moshi.JsonClass

object AccessTokenModel {

    data class Result(val access_token: String, val token_type: String?, val expires_in: String?, val ext_expires_in: String?, val expires_on: String?, val not_before: String?, val resource: String?)

    /**
     * Partial parsing for performance
     * We only need the error_description
     * FYI: Full definition:
     * data class Error(val error: String, val error_description: String, val error_codes: Array<Int>, val timestamp: String, val trace_id: String, val correlation_id: String)
     */
    @JsonClass(generateAdapter = true)
    data class Error(val error: String?, val error_description: String?)


    /**
     * Utility func to parseError from json body
     * Swych access token error has additional error body
     */
    @JvmStatic
    fun parseError(error: Throwable): Error? {
        val errorBody = Util.getHttpErrorBody(error) ?: return null
        val adapter: JsonAdapter<Error> = Factory.moshi.adapter(
            AccessTokenModel.Error::class.java)
        return adapter.fromJson(errorBody)
    }
}