/*
 * Copyright 2019 Swych Inc.
 * Author: Trung Vo
 */

package com.goswych.gc.api

import com.goswych.gc.api.models.AccessTokenModel
import com.goswych.gc.api.models.CatalogModel
import com.goswych.gc.api.models.TransactionModel
import io.reactivex.Observable
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.http.*

/**
 * Low-level REST API
 * Using RxJava async lib
 */
interface GcRestApi {

    /**
     * Async REST method
     */
    @POST("oauth2/token")
    @FormUrlEncoded
    fun apiRequestAccessToken(
        @Header("Ocp-Apim-Subscription-Key") ocpApimSubscriptionKey: String,
        @Field("grant_type") grant_type: String,
        @Field("client_id") client_id: String,
        @Field("client_secret") client_secret: String,
        @Field("resource") resource: String
    ): Observable<AccessTokenModel.Result>

    /**
     * Async REST method
     */
    @GET("api/v1/b2b/{accountId}/catalog")
    fun apiGetCatalog(
        @Header("Ocp-Apim-Subscription-Key") ocpApimSubscriptionKey: String,
        @Header("Authorization") Authorization: String,
        @Header("ApiKey") ApiKey: String,
        @Header("clientId") clientId: String,
        @Path("accountId") accountId: String,
        @Query("pageNo") pageNo: String? = null,
        @Query("pageSize") pageSize: String? = null,
        @Query("countryCode") countryCode: String? = null
    ): Observable<CatalogModel.Result>

    @PUT("api/v1/b2b/order")
    @Headers("Content-Type: application/json")
    fun apiOrderGiftCard(
        @Header("Ocp-Apim-Subscription-Key") ocpApimSubscriptionKey: String,
        @Header("Authorization") Authorization: String,
        @Header("ApiKey") ApiKey: String,
        @Header("clientId") clientId: String,
        @Body content: TransactionModel.OrderGCRequest
    ): Observable<TransactionModel.OrderResult>

    @POST("api/v1/b2b/order")
    @Headers("Content-Type: application/json")
    fun apiOrderConfirm(
        @Header("Ocp-Apim-Subscription-Key") ocpApimSubscriptionKey: String,
        @Header("Authorization") Authorization: String,
        @Header("ApiKey") ApiKey: String,
        @Header("clientId") clientId: String,
        @Body content: TransactionModel.ConfirmOrderRequest
    ): Observable<TransactionModel.ConfirmResult>


    companion object {
        val instance by lazy {
            Factory.create<GcRestApi>("https://api.swychover.io/sandbox/", RxJava2CallAdapterFactory.create())
        }
    }

}