/*
 * Copyright 2019 Swych Inc.
 * Author: Trung Vo
 */

package com.goswych.gc.api

import com.goswych.gc.api.models.AccessTokenModel
import com.goswych.gc.api.models.CatalogModel
import com.goswych.gc.api.models.TransactionModel
import io.reactivex.Observable

/**
 * Async client api with key injection
 * Support Java interoperability
 */
object GcKeyRestApi {

    /**
     * requestAccessToken
     */
    @JvmStatic
    fun requestAccessToken(): Observable<AccessTokenModel.Result> = GcRestApi.instance.apiRequestAccessToken(
        Session.apiKeys.ocpApimSubscriptionKey,
        "client_credentials",
        Session.apiKeys.client_id,
        Session.apiKeys.client_secret,
        Session.apiKeys.resource
    )

    /**
     * getCatalog
     */
    @JvmStatic
    fun getCatalog(
        pageNo: String? = null,
        pageSize: String? = null,
        countryCode: String? = null
    ): Observable<CatalogModel.Result> = GcRestApi.instance.apiGetCatalog(
        Session.apiKeys.ocpApimSubscriptionKey,
        Session.getAccessToken(),
        Session.apiKeys.apiKey,
        Session.apiKeys.clientId,
        Session.apiKeys.accountId,
        pageNo, pageSize, countryCode
    )

    /**
     * orderGiftCard
     */
    @JvmStatic
    fun orderGiftCard(
        brandId: String,
        productId: String,
        amount: Int,
        recipientEmail: String,
        recipientPhoneNumber: String,
        senderPhone: String,
        accessCode: String? = null
    ): Observable<TransactionModel.OrderResult> = GcRestApi.instance.apiOrderGiftCard(
        Session.apiKeys.ocpApimSubscriptionKey,
        Session.getAccessToken(),
        Session.apiKeys.apiKey,
        Session.apiKeys.clientId,
        TransactionModel.OrderGCRequest(
            accountId = Session.apiKeys.accountId,
            brandId = brandId,
            productId = productId,
            amount = amount,
            recipientEmail = recipientEmail,
            recipientPhoneNumber = recipientPhoneNumber,
            notificationDeliveryMethod = 1,
            giftDeliveryMode = 4,
            swychable = true,
            senderPhone = senderPhone,
            accessCode = accessCode
        )
    )

    /**
     * orderConfirm
     */
    @JvmStatic
    fun orderConfirm(
        orderId: String,
        accessCode: String
    ): Observable<TransactionModel.ConfirmResult> = GcRestApi.instance.apiOrderConfirm(
        Session.apiKeys.ocpApimSubscriptionKey,
        Session.getAccessToken(),
        Session.apiKeys.apiKey,
        Session.apiKeys.clientId,
        TransactionModel.ConfirmOrderRequest(
            accountId = Session.apiKeys.accountId,
            accessCode = accessCode,
            orderId = orderId
        )
    )
}