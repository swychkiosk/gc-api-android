/*
 * Copyright 2019 Swych Inc.
 * Author: Trung Vo
 */

package com.goswych.gc.api.models

object TransactionModel {

    data class OrderResult(
        val orderId: String,
        val AccessCode: String,
        val success: Boolean, val message: String?, val statusCode: Int
    )

    data class ConfirmResult(
        val balance: Double,
        val transaction: Transaction?,
//        val Giftcard: GiftCardModel.GiftCard?,
        val success: Boolean, val message: String?, val statusCode: Int
    )

    data class MessageInApp (
        val message: String?,
        val imageUrl: String?,
        val videoUrl: String?
    )

    data class Transaction (
        val transactionId: String?,
        val parentTransactionId: String?,
        val transactionDate: String?,
        val redeemOrSaveDate: String?,
        val transactionType: Int?,
        val amount: String?,
        val recipientPhoneNumber: String?,
        val recipientEmail: String?,
        val recipientName: String?,
        val recipientSwychId: String?,
        val status: Int?,
        val brand: CatalogModel.Brand?,
// not sample for this object
//        val payment:
        val messageFromSender: MessageInApp,
        val senderPhoneNumber: String?,
        val senderEmail: String?,
        val senderName: String?,
        val senderSwychId: String?,
        val statusDisplay: String?,
// not sample for this object
//        val previousPaymentType
        val orderId: String?,
        val showAttachment: Boolean?,
        val foreignCurrency: String?,
        val foreignAmount: Int?,
        val DeliveryType: Int?
    )

    data class OrderGCRequest(
        val accountId: String,
        val brandId: String,
        val productId: String,
        val amount: Int,
        val recipientEmail: String?,
        val recipientPhoneNumber: String?,
        val notificationDeliveryMethod: Int?,
        val giftDeliveryMode: Int?,
        val swychable: Boolean?,
        val senderPhone: String?,
        val accessCode: String?,
        val deliverGiftTo: Int? = null
    )

    data class ConfirmOrderRequest(
        val accountId: String?,
        val orderId: String?,
        val accessCode: String?
    )
}