/*
 * Copyright 2019 Swych Inc.
 * Author: Trung Vo
 */

package com.goswych.gc.api.models

object CatalogModel {
    data class Result(
        val categoryList: Array<String?>?,
        val catalog: Array<Brand>?,
        val success: Boolean, val message: String?, val statusCode: Int
    )

    data class Brand(
        val brandId: String,
        val brandName: String?,
        val brandImage: String?,
        val brandTC: String?,
        val brandDisclaimer: String?,
        val brandRedemptionInstruction: String?,

        val categories: Array<Int>?,
        val products: Array<Product>?,

        val canSendToFriend: Boolean?,
        val canSelfGifting: Boolean?,
        val canSwych: Boolean?,
        val canRedeem: Boolean?,
        val canRegift: Boolean?,
        val canCashout: Boolean?,

        val countryCode: String?,
        val currency: String?,
        val currencySymbol: String?,
        val claimURL: String?,
        val availability: Int?
    )

    data class Product(
        val productId: String?, val productName: String?, val productType: String?, val amount: String?,
        val minimumAmount: String?, val maximumAmount: String?, val currencySymbols: String?
    )

    // Todo what are error classes?
}